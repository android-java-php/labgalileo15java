
package labgalileo15;

import org.Galileo.controlpanel.LabGl15;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 */
public class LabGalileo15 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     new LabGl15().setVisible(true);
    }
    
}
